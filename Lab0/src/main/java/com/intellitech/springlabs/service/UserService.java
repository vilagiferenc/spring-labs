package com.intellitech.springlabs.service;

import com.intellitech.springlabs.model.User;

public interface UserService {

	User findByUsernameOrEmail(String usernameOrEmail);
}
